from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TaskForm

# Create your views here.


def todo_list_view(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list": todo_list,
    }
    return render(request, "todos/list.html", context)


def show_todo_list(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list": todo_list,
    }
    return render(request, "todos/detail.html", context)


def create_todo_list(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def update_todo_list(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todo_list)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoForm(instance=todo_list)

    context = {"form": form}
    return render(request, "todos/edit.html", context)


def delete_todo_list(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def create_todo_item(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TaskForm()

    context = {
        "form": form,
    }
    return render(request, "todos/items/create.html", context)


def update_todo_item(request, id):
    todo_item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TaskForm(request.POST, instance=todo_item)
        if form.is_valid():
            todo_item = form.save()
            return redirect("todo_list_detail", id=todo_item.id)
    else:
        form = TaskForm(instance=todo_item)

    context = {"form": form}
    return render(request, "todos/items/edit.html", context)
